# SPDX-License-Identifier: LGPL-3.0-or-later
#
# Copyright (C) 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2023
#   Guillaume Tucker <guillaume.tucker@gmail.com>

"""Splat setuptools residual file for building the _splat.so extension"""

import os
from setuptools import Extension, setup

_splat_sources = list(
    os.path.join('src', src) for src in [
        '_splat.c',
        'filter.c',
        'frag.c',
        'mmap.c',
        'signal.c',
        'sine_table.c',
        'spline.c',
        'source.c',
    ]
)

setup(
    ext_modules=[
        Extension(
            name='_splat', sources=_splat_sources, depends=['src/_splat.h']
        ),
    ]
)
